for i in `ls /mnt/ocean/RawData/JGI_fastq/RNA/RNA_no_rRNA/cut_adapt_sickle/*.PE.fq.gz | awk -F "/" '{print $9}' | sed "s/.PE.fq.gz//g"` 
do
bowtie2 -p 12 --very-sensitive-local --dovetail -x Sum22DL08.bin33 --interleaved /mnt/ocean/RawData/JGI_fastq/RNA/RNA_no_rRNA/cut_adapt_sickle/$i.PE.fq.gz -S $i.Sum22DL08.bin33.sam --no-unal
done

